let mongoose = require('mongoose')
let Schema = mongoose.Schema

const ReportSchema = new Schema({
  _id: { type: String, required: true },
  sha: String,
  repo: String,
  branch: String,
  author: {
    name: String,
    email: String,
    date: Date
  },
  committer: {
    name: String,
    email: String,
    date: Date
  },
  fileCount: Number,
  stats: {
    total: Number,
    additions: Number,
    deletions: Number
  },
  message: String,
  dateCollected: Date
}, { collection: 'github-report' })

mongoose.model('Report', ReportSchema)
