const chalk = require('chalk')
const log = console.log

const outputToScreen = commitInfo => {
  const { repo, branch, author, committer, stats, fileCount, message } = commitInfo

  if (author.name !== committer.name) {
    // This is a merge
    log(chalk`{bgHex('#0786D5')  {whiteBright ${repo} }}{bgWhite  {purple ᚶ ${branch}} } {gray ${committer.date}} {green ∞} :: Files: {cyan ${fileCount}} {greenBright Merged branch} -- ${author.name}`)
  } else
    log(chalk`{bgHex('#0786D5')  {whiteBright ${repo} }}{bgWhite  {black ᚶ ${branch}} } {gray ${committer.date}} {green +${stats.additions}}/{red -${stats.deletions}}={blue ${stats.total}} :: Files: {cyan ${fileCount}} {gray ${message}} -- {yellow ${author.name} }`)
}

module.exports = outputToScreen
