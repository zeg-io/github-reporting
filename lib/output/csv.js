const outputToCSV = (commitInfo, writeStream) => {
  const { repo, branch, author, committer, stats, fileCount, message } = commitInfo

  let line

  if (author.name !== committer.name) {
    // This is a merge
    line = `${repo},${branch},${committer.date},0,0,0,${fileCount},"${message}","${author.name}"\n`
  } else
    line = `${repo},${branch},${committer.date},${stats.additions},${stats.deletions},${stats.total},${fileCount},"${message}","${author.name}"\n`
  writeStream.write(line, 'utf8')
}

module.exports = outputToCSV
