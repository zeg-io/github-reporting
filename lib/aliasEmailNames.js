// FROM -> TO

const aliasArray = [
  ['V644558', 'Laith Albataineh'],
  ['Reidho', 'Horrace Reid'],
  ['William Johnson8', 'Drew Johnson'],
  ['Beachre', 'Rebecca Beach'],
  ['Kinsjo', '????'],
  ['Lausifr', 'Fredrick Lausier'],
  ['Colshacol', 'Colton Colcleasure'],
  ['Bobbjo', 'Joshua Bobb'],
  ['Banerda', 'Dave Banerjee'],
  ['Condiau', 'Austin Condiff'],
  ['Fazelke', 'Keya Fazelinia'],
  ['Guccijo', 'Jon Gucciardi'],
  ['Abc', 'Jon Gucciardi'],
  ['Jon Gucciardo', 'Jon Gucciardi'],
  ['Thoja8f', 'Jason Thompson'],
  ['Jason Thompson4', 'Jason Thompson'],
  ['Jackeb4', 'Ebony Jackson'],
  ['Ebony Jackson2', 'Ebony Jackson']
]

const manualCrappyWorkAroundToInconsistantData = name => {
  const aliasesLength = aliasArray.length

  for (let i = 0; i < aliasesLength; i++) {
    if (aliasArray[i][0] === name)
      return aliasArray[i][1]
  }
  return name
}

module.exports = manualCrappyWorkAroundToInconsistantData
