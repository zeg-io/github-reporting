const fetch = require('./fetch'),
      pRetry = require('p-retry')

const gitApi = 'https://github.vcp.vzwnet.com/api/v3'

const run = path => () => {
  return fetch('GET', `${path}`)
}
// const get = path => fetch('GET', `${gitApi}${path}`)
const get = path => pRetry(run(`${gitApi}${path}`), { retries: 5 })

// REPOS
const basicProjectDetails = ownerProject =>     get(`/repos/${ownerProject}`)
const releases = ownerProject =>                get(`/repos/${ownerProject}/releases`)
const branches = ownerProject =>                get(`/repos/${ownerProject}/branches`)
const branch = (ownerProject, branchName) =>    get(`/repos/${ownerProject}/branches/${branchName}`)
const commits = (ownerProject, sha, page = 1) => get(`/repos/${ownerProject}/commits?sha=${sha}&per_page=100&page=${page}&since=2016-12-31T00:00:00Z`)
const commit = (ownerProject, commitSha) =>     get(`/repos/${ownerProject}/commits/${commitSha}`)

// --STATS
const stats = (ownerProject, apiCall) =>        get(`/repos/${ownerProject}/stats/${apiCall}`)
const punchCard = ownerProject =>               stats(ownerProject, 'punch_card')
const participation = ownerProject =>           stats(ownerProject, 'participation')
const contributors = ownerProject =>            stats(ownerProject, 'contributors')

// USERS
const userRoot = userName =>                    get(`/users/${userName}`)
const starred = userName =>                     get(`/users/${userName}/starred`)

const buildApiCallPromiseArray = (promiseArray, apiCallFunction, ownerProject, objectPropertyName) => {
  promiseArray.push(apiCallFunction(ownerProject)
  .then(response => {
    let reply = {}

    reply[ownerProject] = {}
    reply[ownerProject][objectPropertyName] = response
    return reply
  }))
}

module.exports = {
  repo: {
    basicDetails: basicProjectDetails,
    releases,
    branch,
    branches,
    commit,
    commits,
    buildApiCallPromiseArray,
    stats: {
      punchCard,
      participation,
      contributors
    }
  },
  user: {
    get: userRoot,
    starred
  }
}
