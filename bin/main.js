const chalk = require('chalk'),
      fs = require('fs'),
      db = require('../models'),
      moment = require('moment'),
      Spinner = require('cli-spinner').Spinner
const userQuery = require('../lib/query/user'),
      repoQuery = require('../lib/query/repo')

// OUTPUT OPTIONS
const screenOutput = require('../lib/output/screen'),
      csvOutput = require('../lib/output/csv'),
      htmlOutput = require('../lib/output/html'),
      mongoOutput = require('../lib/output/mongo'),
      mongoDataForHtml = require('../lib/output/mongoDataForHtml'),
      callWriteHtml = require('../lib/callWriteHtml'),
      promiseSyncronous = require('../lib/promiseSyncronous')
const report = db.mongoose.model('Report'),
      connectMongoDb = db.connectMongoDb

const log = console.log

let spinner = new Spinner('Polling GitHub... %s')

spinner.setSpinnerString('|/-\\')

let data = {},
    htmlData = {
      labels: [],
      users: {},
      colorIndex: 0,
      raw: []
    }

const getGitHubApiData = (userName, { outputScreen, outputCsv, outputMongo, outputHtml, startDate, endDate, noSpinner }, writeStream, saveToFileName, bulkOperator) =>
  userQuery(userName)
  .then(userData => {
    data.user = userData.root
    data.repos = userData.starred

    return promiseSyncronous(data.repos, repo => {
      log(chalk.cyan(repo.fullName))
      return repoQuery(repo.fullName, spinner)
    })
    .then(repoLogs => {
      if (!noSpinner) spinner.start()

      spinner.setSpinnerTitle('%s Preparing to Output...')

      log(chalk`{yellowBright Beginning output...}`)
      log(`Repos - ${repoLogs.length}`)


      return promiseSyncronous(repoLogs, repoLog => {
        log(`Repo Branches - ${repoLog.length}`)
        return promiseSyncronous(repoLog, logEntry => {
          log(`Commit Details - ${logEntry.length}`)
          return Promise.all(logEntry.map(commitDetails => {
            // OUTPUT SECTION
            if (outputCsv)
              return csvOutput(commitDetails, writeStream)
            // if (outputHtml)
            //   htmlOutput(commitDetails, htmlData)
            if (outputMongo)
              return mongoOutput(commitDetails, userName, bulkOperator, spinner, noSpinner)
          }))
        })
      })
    })
  })
  .catch(err => {
    log(chalk.red(`\nError communicating with GitHub Server: ${err.message}`))
    log(chalk.red('Failed.'))
    console.dir(err)
  })

const main = (userName, { outputScreen, outputCsv, outputMongo, outputHtml, readOnly, startDate, endDate, noSpinner }, saveToFileName, mongoURI, databaseName = 'github-reporting') => {
  let writeStream

  log(`${chalk.green('Reporting on starred repos for ')}${chalk.blue.bold(userName)}`)

  if (outputMongo) {
    db.init(mongoURI, {})
  }
  if (outputCsv) {
    if (!noSpinner) spinner.start()

    writeStream = fs.createWriteStream(saveToFileName)
    writeStream.write('Repo,Branch,"Commit Date","Lines Added","Lines Removed","Lines Changed","Files Changed",Comment,Committer\n', 'utf8')
    writeStream.on('finish', () => {
      log(chalk`{greenBright Report completed, wrote data to ${saveToFileName}}`)
    })
    getGitHubApiData(userName, { outputScreen, outputCsv, outputMongo, outputHtml, startDate, endDate, noSpinner }, writeStream, saveToFileName)
    .then(() => {
      writeStream.end()
    })
  } else if (outputMongo && !readOnly) {
    if (!noSpinner) spinner.start()

    if (!mongoURI)
      return log(chalk`{red Missing Mongo URI. Proper usage: --mongo mongodb://user:password@server:27017/database}`)

    connectMongoDb()
    .then(mongoDbconnection => {
      const mongoDb = mongoDbconnection.db('github-reporting'),
            dbCollection = mongoDb.collection('github-report'),
            bulk = dbCollection.initializeUnorderedBulkOp()

      return getGitHubApiData(userName, { outputScreen, outputCsv, outputMongo, outputHtml, startDate, endDate, noSpinner }, writeStream, saveToFileName, bulk)
      .then(() => {
        return bulk.execute()
        .then(out => {
          log(chalk`{cyan Inserted: ${out.nInserted}; Matched: ${out.nMatched}; Modified: ${out.nModified}; Removed: ${out.nRemoved}; Upserted: ${out.nUpserted}}`)
        })
        .catch(err => {
          err.writeErrors.forEach(error => {
            log(chalk`{red WARNING: ${error.errmsg}}`)
            console.dir(error.toString())
          })
        })
      })
      .then(() => {
        log(chalk`{greenBright Done writing data.}`)

        log(chalk`{greenBright Bulk Upsert completed.}`)

        if (outputHtml)
          return callWriteHtml(spinner, saveToFileName)
      })
      .then(() => {
        mongoDb.disconnect()
        log(chalk`{greenBright Bulk Upsert connection closed.}`)
      })
    })
    .catch(error => {
      log(chalk`{red ${error.message}}`)
    })
  } else if (outputHtml)
    callWriteHtml(spinner, saveToFileName, startDate, endDate)
}

module.exports = main
